SHELL := /bin/bash

#name of the executable
TARGET  := painter

SOURCEDIR := src
HEADERDIR := src
BUILDDIR  := build
INCLUDEDIR := include

SOURCES := $(shell find -name '*.cpp')
OBJECTS := $(subst $(SOURCEDIR),$(BUILDDIR),$(SOURCES:.cpp=.o))

CC      := g++
CFLAGS  := -std=c++20 -g -lmenu -lncursesw

all: $(TARGET)

DEPS = $(OBJECTS:.o=.d)
-include $(DEPS)
DEPFLAGS = -MMD -MF $(@:.o=.d)

$(TARGET): $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $(TARGET)

$(OBJECTS): $(BUILDDIR)/%.o : $(SOURCEDIR)/%.cpp
	$(CC) $(CFLAGS) -I$(INCLUDEDIR) -I$(HEADERDIR) -I$(dir $<) $(DEPFLAGS) -c $< -o $@

clean:
	rm -f $(TARGET) $(OBJECTS) $(DEPS)
